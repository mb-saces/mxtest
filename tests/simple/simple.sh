#!/bin/sh

set -eu
#set -o pipefail # POSIX:2022, oldstable sucks :(


# wipe old data from previous run
mxtest purge


# initialise 
mxtest init


# Adjust configuration to your needs, defaults are fine for local testing.


# include configuration
. ./.env


# configure services you want to use
${MXTEST_SDK_ROOT}/hs/synapse/setup.sh


# start and wait for healthy
mxcompose up -d --wait


# do something with service
mxcompose exec synapse register_new_matrix_user -c /data/homeserver.yaml -u testadmin -p testadmin -a

mxcompose exec synapse register_new_matrix_user -c /data/homeserver.yaml -u testuser -p testuser --no-admin


# show logs
mxcompose logs synapse-db
mxcompose logs synapse

# shut down
mxcompose down

echo "thx, bye."
