#!/bin/sh

set -eu
#set -o pipefail # POSIX:2022, oldstable sucks :(

. ./.env


mkdir -p ${MXTEST_HOME}/data/synapse
mkdir -p ${MXTEST_HOME}/data/synapse-db

mxdocker run --rm \
    --mount type=bind,src=${MXTEST_HOME}/data/synapse,dst=/data \
    -e SYNAPSE_SERVER_NAME=synapse \
    -e SYNAPSE_REPORT_STATS=no \
    --user ${MXTEST_UID}:${MXTEST_GID} \
    docker.io/matrixdotorg/synapse:latest generate

mxdocker run --rm \
    --mount type=bind,src=${MXTEST_HOME}/data/synapse,dst=/data \
    --mount type=bind,src=${MXTEST_SDK_ROOT}/hs/synapse,dst=/scripts \
    --user ${MXTEST_UID}:${MXTEST_GID} \
    --entrypoint /bin/bash \
    docker.io/matrixdotorg/synapse:latest -c "python /scripts/setupdb.py /data/homeserver.yaml"
