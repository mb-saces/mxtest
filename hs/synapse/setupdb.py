import sys
import yaml

def confighacker(hsconfig):
    with open(hsconfig, 'r') as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    cfg['database'] = {
        'name': 'psycopg2',
        'txn_limit': 10000,
        'args': {
            'user': 'synapse',
            'password': 'password',
            'database': 'synapse',
            'host': '/var/run/postgresql',
            'cp_min': 5,
            'cp_max': 10,
        }
    }

    with open(hsconfig, 'w') as f:
        yaml.dump(cfg, f)

if __name__ == "__main__":
    confighacker(sys.argv[1])
