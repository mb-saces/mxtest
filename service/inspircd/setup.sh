#!/bin/sh

set -eu
#set -o pipefail # POSIX:2022, oldstable sucks :(

. ./.env

mkdir -p ${MXTEST_HOME}/data/inspircd/conf
mkdir -p ${MXTEST_HOME}/data/thelounge/

docker run --rm \
    --mount type=bind,src=${MXTEST_HOME}/data/inspircd/,dst=/data \
    docker.io/library/alpine:latest /bin/sh -c "chown -R 10000:10000 /data/conf"

cp ${MXTEST_SDK_ROOT}/service/inspircd/thelounge/config.js ${MXTEST_HOME}/data/thelounge/

docker run --rm \
    --mount type=bind,src=${MXTEST_HOME}/data/,dst=/data \
    docker.io/library/alpine:latest /bin/sh -c "chown -R 1000:1000 /data/thelounge"
