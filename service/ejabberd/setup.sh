#!/bin/sh

set -eu
#set -o pipefail # POSIX:2022, oldstable sucks :(

. ./.env

mkdir -p ${MXTEST_HOME}/data/ejabberd/conf
mkdir -p ${MXTEST_HOME}/data/ejabberd/database
mkdir -p ${MXTEST_HOME}/data/ejabberd/logs
mkdir -p ${MXTEST_HOME}/data/ejabberd/upload

cp ${MXTEST_SDK_ROOT}/service/ejabberd/ejabberd.yml ${MXTEST_HOME}/data/ejabberd/conf/ejabberd.yml

docker run --rm \
    --mount type=bind,src=${MXTEST_HOME}/data/,dst=/data \
    docker.io/library/alpine:latest /bin/sh -c "chown -R 9000:9000 /data/ejabberd"
